'use strict'

function breadcrumbsOverflown () {
  var element = document.getElementById('breadcrumbs')
  return element.scrollHeight > element.clientHeight || element.scrollWidth > element.clientWidth
}

function getBreadcrumbs () {
  return document.getElementById('breadcrumbs').getElementsByTagName('LI')
}

function getVisibleBreadcrumbs () {
  var breadcrumbs = getBreadcrumbs()
  var visibleBreadcrumbs = []
  for (var i = 0; i < breadcrumbs.length; i++) {
    var breadcrumb = breadcrumbs[i]
    var item = breadcrumb.getElementsByClassName('item')[0]
    if (item.style.display === 'inline') {
      visibleBreadcrumbs.push(breadcrumb)
    }
  }
  return visibleBreadcrumbs
}

function resetBreadcrumbs () {
  var breadcrumbs = getBreadcrumbs()
  for (var i = 0; i < breadcrumbs.length; i++) {
    var breadcrumb = breadcrumbs[i]
    var item = breadcrumb.getElementsByClassName('item')[0]
    var ellipsis = breadcrumb.getElementsByClassName('ellipsis')[0]
    item.style.display = 'inline'
    ellipsis.style.display = 'none'
  }
}

function hideMiddleVisibleBreadcrumb () {
  var breadcrumbs = getVisibleBreadcrumbs()
  var i = parseInt(breadcrumbs.length / 2 - (breadcrumbs.length % 2 === 0 ? 1 : 0))
  var breadcrumb = breadcrumbs[i]
  var item = breadcrumb.getElementsByClassName('item')[0]
  var ellipsis = breadcrumb.getElementsByClassName('ellipsis')[0]
  item.style.display = 'none'
  ellipsis.style.display = 'inline'
}

function ellipsizeBreadcrumbs () {
  resetBreadcrumbs()
  var i = 0 // prevent infinite loop in case something goes wrong
  while (breadcrumbsOverflown() && getVisibleBreadcrumbs().length > 2 && i < 20) {
    hideMiddleVisibleBreadcrumb()
    i++
  }
}

document.addEventListener('DOMContentLoaded', function () {
  ellipsizeBreadcrumbs()
})

window.addEventListener('resize', function () {
  ellipsizeBreadcrumbs()
})
