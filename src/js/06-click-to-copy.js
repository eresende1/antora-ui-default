;(function () {
  var codes = document.querySelectorAll('div.listingblock:not(.output) div.content code')
  for (var codeI = 0; codeI < codes.length; codeI++) {
    var code = codes[codeI]
    var tools = document.createElement('span')
    tools.classList.add('codeTools')
    var lang = code.dataset.lang
    var sourceLang = document.createElement('span')
    sourceLang.innerText = lang.toUpperCase()
    var copyButton = document.createElement('button')
    copyButton.classList.add('copyButton')
    copyButton.innerHTML = '<svg class="clipboardIcon" style="pointer-events: none" xmlns="http://www.w3.org/2000/svg" fill="#808080" width="0.9rem" height="0.9rem" viewBox="0 0 52 52" enable-background="new 0 0 52 52" xml:space="preserve"><g><path d="M17.4,11.6h17.3c0.9,0,1.6-0.7,1.6-1.6V6.8c0-2.6-2.1-4.8-4.7-4.8h-11c-2.6,0-4.7,2.2-4.7,4.8V10   C15.8,10.9,16.5,11.6,17.4,11.6z"/><path d="M43.3,6h-1.6c-0.5,0-0.8,0.3-0.8,0.8V10c0,3.5-2.8,6.4-6.3,6.4H17.4c-3.5,0-6.3-2.9-6.3-6.4V6.8   c0-0.5-0.3-0.8-0.8-0.8H8.7C6.1,6,4,8.2,4,10.8v34.4C4,47.8,6.1,50,8.7,50h34.6c2.6,0,4.7-2.2,4.7-4.8V10.8C48,8.2,45.9,6,43.3,6z"/></g></svg>'
    var copyToast = document.createElement('span')
    copyToast.classList.add('copyToast')
    copyToast.innerText = 'Copied'
    copyButton.appendChild(copyToast)
    tools.appendChild(sourceLang)
    tools.appendChild(copyButton)
    code.appendChild(tools)
  }
  document.addEventListener('click', function (e) {
    e.stopPropagation()
    console.debug(e.target)
    if (e.target.classList.contains('copyButton')) {
      var tools = e.target.parentNode
      var code = tools.parentNode
      var copyToast = tools.lastChild.lastChild
      copyToast.style.opacity = '1'
      setTimeout(function () { copyToast.style.opacity = '0' }, 800)
      var text = code.innerText
      text = text.substring(0, text.lastIndexOf('\n')) // removes the toast's text ("Copied")
      text = text.substring(0, text.lastIndexOf('\n')) // removes the language encodding
      window.navigator.clipboard.writeText(text)
    }
  })
})()
