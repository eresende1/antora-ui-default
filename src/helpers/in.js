'use strict'

module.exports = (arr, item) => arr && arr.indexOf(item) >= 0
