'use strict'

module.exports = (name, value, options) => {
  if (!options.data.root) {
    options.data.root = {}
  }
  options.data.root[name] = value
  return ''
}
