'use strict'

var nextId = 0

module.exports = () => nextId++
