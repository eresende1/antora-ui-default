'use strict'

module.exports = (arr, i) => arr && (i - 1) < arr.length && arr[i - 1]
